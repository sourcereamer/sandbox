package com.sandbox

/*
* add function contains two Int, returns sum of them as an Int.
* */
class Calculator {
    fun add(a: Int, b: Int): Int {
        return a + b
    }
}
